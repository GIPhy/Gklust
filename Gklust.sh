#!/bin/bash

#############################################################################################################
#                                                                                                           #
# Gklust: fast greedy clustering of genomes based on minhash similarity                                     #
#                                                                                                           #
# Copyright (C) 2019  Alexis Criscuolo                                                                      #
#                                                                                                           #
# This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
# General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
# (at your option) any later version.                                                                       #
#                                                                                                           #
# This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
# License for more details.                                                                                 #
#                                                                                                           #
# You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
# <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                           #
#  Contact:                                                                                                 #
#  Institut Pasteur                                                                                         #
#  Bioinformatics and Biostatistics Hub                                                                     #
#  C3BI, USR 3756 IP CNRS                                                                                   #
#  Paris, FRANCE                                                                                            #
#                                                                                                           #
#  alexis.criscuolo@pasteur.fr                                                                              #
#                                                                                                           #
#############################################################################################################

#############################################################################################################
#                                                                                                           #
# ============                                                                                              #
# = VERSIONS =                                                                                              #
# ============                                                                                              #
#                                                                                                           #
  VERSION=0.1.190513ac                                                                                      #
# +                                                                                                         #
#                                                                                                           #
#############################################################################################################

#############################################################################################################
#                                                                                                           #
# ============                                                                                              #
# = DOC      =                                                                                              #
# ============                                                                                              #
#                                                                                                           #
  if [ "$1" = "-?" ] || [ "$1" = "-h" ] || [ $# -le 1 ]                                                     #
  then                                                                                                      #
    cat << EOF
   __  _  __ _   _  _   __  _____  
  / _]| |/ /| | | || |/' _/|_   _| 
 | [/\|   < | |_| || |'._'.  | |   
  \__/|_|\_\|___|____||___/  |_|  v.$VERSION   

 USAGE:
    Gklust.sh  [options]
 where:
    -i <file>   input file containing the FASTA-formatted genome file names (one
                per line) to process (mandatory)
    -r <file>   input file  containing the  FASTA-formatted  genome file name(s)
                (one per line) to be used as reference genome(s)
    -c <real>   minhash similarity clustering cutoff (in percent; default: 95)
    -s <int>    minhash sketch size (default: 1000)
    -k <int>    minhash k-mer size (default: 21)
    -t <int>    number of threads (default: 2)

EOF
    exit 1 ;                                                                                                #
  fi                                                                                                        #
#                                                                                                           #
#############################################################################################################

  
#############################################################################################################
#                                                                                                           #
# ================                                                                                          #
# = INSTALLATION =                                                                                          #
# ================                                                                                          #
#                                                                                                           #
# [1] REQUIREMENTS =======================================================================================  #
#  Gklust depends on Mash. You should have it installed on your computer prior to using Gklust. Make sure   #
#  that it is installed on your $PATH variable, or specify below the full path to the Mash binary.          #
#                                                                                                           #
# -- Mash: fast pairwise minhash dissimilarity estimation ---------------------------------------------     #
#    + binaries: github.com/marbl/Mash/releases                                                             #
#    + Ondov BD, Treangen TJ, Melsted P, Mallonee AB, Bergman NH, Koren S, Phillippy AM (2016) Mash:        #
#        fast genome and  metagenome distance estimation using MinHash. Genome Biology, 17:132. doi:        #
#        10.1186/s13059-016-0997-x                                                                          #
#                                                                 ################################################
                                                                  ################################################
  MASH=mash;                                                      ## <=== WRITE HERE THE PATH TO THE MASH       ##
                                                                  ##      BINARY (VERSION 1.0.2 MINIMUM)        ##
                                                                  ################################################
                                                                  ################################################
#                                                                                                           #
#                                                                                                           #
# [2] EXECUTE PERMISSION =================================================================================  #
#  In order to run Gklust, give the execute permission on the script Gklust.sh:                             #
#    chmod +x Gklust.sh                                                                                     #
#                                                                                                           #
#############################################################################################################

  
#############################################################################################################
#                                                                                                           #
# =============                                                                                             #
# = CONSTANTS =                                                                                             #
# =============                                                                                             #
#                                                                                                           #
  WAITIME=0.1;                                                                                              #
  MAXINT=1; b=16; while [ $MAXINT -gt 0 ]; do MAXINT=$(( 2**(++b) - 1 )); done; MAXINT=$(( 2**(--b) - 1 )); #
  G2KSRC='int c[256],b[4096],*x,*y,s;float cg;int main(){while((y=b+(read(0,b,sizeof b)/sizeof(int)))>b)for(x=b;x<y;x++){c[(*x)&0xff]++;c[(*x>>8)&0xff]++;c[(*x>>16)&0xff]++;c[(*x>>24)&0xff]++;}printf("%f\n",(s=c['\''A'\'']+c['\''a'\'']+c['\''T'\'']+c['\''t'\'']+(cg=c['\''C'\'']+c['\''c'\'']+c['\''G'\'']+c['\''g'\'']))/100000+cg/s);}';
# C source code above adapted from: https://tinyurl.com/yxfyr9lx                                            #
#                                                                                                           #
#############################################################################################################


#############################################################################################################
#############################################################################################################
#### INITIALIZING PARAMETERS AND READING OPTIONS                                                         ####
#############################################################################################################
#############################################################################################################

if [ ! $(command -v $MASH) ]; then echo "$MASH not found"   >&2 ; exit 1 ; fi
if [ ! $(command -v awk) ];   then echo "awk not found"     >&2 ; exit 1 ; fi
   
export LC_ALL=C ;

NOFILE="N.O.F.I.L.E".$RANDOM; while [ -e $NOFILE ]; do NOFILE="N.O.F.I.L.E".$RANDOM;  done
INFILE=$NOFILE;   # -i (mandatory)
REFFILE=$NOFILE;  # -r 
SKETCH=1000;      # -s 
K=21;             # -k 
CUTOFF=95;        # -c
NPROC=2;          # -t

while getopts :i:r:s:k:c:t: option
do
  case $option in
    i) INFILE="$OPTARG"                                   ;;
    r) REFFILE="$OPTARG"                                  ;;
    s) SKETCH=$OPTARG                                     ;;
    k) K=$OPTARG                                          ;;
    c) CUTOFF=$(tr -d '%'<<<"$OPTARG")                    ;;
    t) NPROC=$OPTARG                                      ;;
    :) echo "option $OPTARG : missing argument" ; exit 1  ;;
   \?) echo "$OPTARG : option invalide" ;         exit 1  ;;
  esac
done

if [ "$INFILE" == "$NOFILE" ];                         then echo "missing input file (option -i)"                                          >&2 ; exit 1 ; fi
if [ ! -e "$INFILE" ];                                 then echo "input file does not exist: $INFILE (option -i)"                          >&2 ; exit 1 ; fi
if [ "$REFFILE" != "$NOFILE" ] && [ ! -e "$REFFILE" ]; then echo "input file does not exist: $REFFILE (option -r)"                         >&2 ; exit 1 ; fi
if ! [[ $SKETCH =~ ^[0-9]+$ ]];                        then echo "incorrect sketch size: $SKETCH (option -s)"                              >&2 ; exit 1 ; fi
if [ $SKETCH -le 100 ];                                then echo "sketch size $SKETCH is too low (option -s)"                              >&2 ; exit 1 ; fi
if ! [[ $K =~ ^[0-9]+$ ]];                             then echo "incorrect k-mer size: $K (option -k)"                                    >&2 ; exit 1 ; fi
if [ $K -le 5 ];                                       then echo "k-mer size $K is too low (option -k)"                                    >&2 ; exit 1 ; fi
if [ $K -gt 32 ];                                      then echo "k-mer size $K is too high (option -k)"                                   >&2 ; exit 1 ; fi
if ! [[ $CUTOFF =~ ^[0-9]+([.][0-9]+)?$ ]];            then echo "incorrect cutoff value: $CUTOFF (option -c)"                             >&2 ; exit 1 ; fi
if [ $(bc <<<"0<=$CUTOFF&&$CUTOFF<=100") -eq 0 ];      then echo "percentage similarity cutoff value should belong to ]0,100[ (option -c)" >&2 ; exit 1 ; fi
if ! [[ $NPROC =~ ^[0-9]+$ ]];                         then echo "incorrect number of threads: $NPROC (option -c)"                         >&2 ; exit 1 ; fi
[ $NPROC -le 1 ] && NPROC=2;
THRESHOLD=$(bc -l<<<"scale=4;1-$CUTOFF/100" | sed 's/^\./0./');

echo "$NPROC threads" ;
echo "k-mer size: $K" ;
echo "sketch size: $SKETCH" ;
echo "cutoff=$CUTOFF% ($THRESHOLD)" ;

CLUSTERS=$INFILE.clust;
MSH=$INFILE.msh;        
r=$RANDOM;
SRT=$INFILE.$r.srt;
SSZ=$INFILE.$r.ssz;
TMP=$INFILE.$r.tmp;
while [ -e $SRT ] || [ -e $SSZ ] || [ -e $TMP ]
do
  r=$RANDOM;
  SRT=$INFILE.$r.srt;
  SSZ=$INFILE.$r.ssz;
  TMP=$INFILE.$r.tmp;
done

trap  "rm -f $SSZ $SRT $TMP $MSH $CLUSTERS ; exit 1"  INT ;


#############################################################################################################
#############################################################################################################
#### PARSING INPUT FILES                                                                                 ####
#############################################################################################################
#############################################################################################################

echo -n "parsing FASTA files ..." >&2 ;
i=0; s=$(( $(cat $INFILE | wc -l) / 20 )); n=0;
max=0;
tr ' ;|' '_' < $INFILE |
  while read -r f
  do
    let i++; if [ $i -ge $n ]; then echo -n "." >&2 ; n=$(( $n + $s )); fi  
    [ -e $f ] && ls -s $f ;
    l=${#f}; [ $max -lt $l ] && max=$l && echo $max > $SSZ ;
  done > $TMP
max=$(head -1 $SSZ);
if [ -e $REFFILE ]
then
  tr ' ;|' '_' < $REFFILE |
    while read f
    do
      [ ! -e $f ] && continue ;
      sed -i "\| $f$|d" $TMP ;
      echo "$MAXINT $f" ;
    done > $SSZ
  cat $TMP >> $SSZ ;
  mv $SSZ $TMP ;
fi
sort -g -r $TMP | tr ' ' '\t' > $SSZ ;
echo " [ok]" >&2 ;
echo "$(cat $SSZ | wc -l) input files" ;


#############################################################################################################
#############################################################################################################
#### SKETCHING GENOME SEQUENCES                                                                          ####
#############################################################################################################
#############################################################################################################

let NPROC--;
( while read -r _ f
 do
   nice $MASH sketch -o $f -s $SKETCH -k $K $f &> /dev/null &
   while [ $(jobs -r | wc -l) -gt $NPROC ]; do sleep $WAITIME ; done
 done < $SSZ
) & pid=$!

trap  INT ;
function ctrl_c() {
  kill -9 $pid 2> /dev/null > /dev/null ;
  sleep 5 ;
  tr ' ;|' '_' < $SSZ | while read -r f ; do rm -f $f.msh ; done
  rm -f $SSZ $SRT $TMP $MSH $CLUSTERS a.out ;
  exit 1 ;
}
trap  ctrl_c  INT ;


#############################################################################################################
#############################################################################################################
#### SORTING GENOME FILES                                                                                ####
#############################################################################################################
#############################################################################################################

echo -n "sorting FASTA files ..." >&2 ;
i=0; s=$(( $(cat $INFILE | wc -l) / 20 )); n=0;
if [ ! $(command -v gcc) ]
then
  while read -r m f
  do
    let i++; if [ $i -ge $n ]; then echo -n "." >&2 ; n=$(( $n + $s )); fi  
    [ $m -eq $MAXINT ] && echo -e "$m\t$f" && continue ;
    s=$(grep -v "^>" $f | tr -cd ACGTacgt | wc -c);
    gc=$(grep -v "^>" $f | tr -cd CGcg | wc -c); 
    echo -e "$(( $s / 100000 ))$(bc <<<"scale=9;$gc/$s")\t$f" ;
  done < $SSZ > $TMP
else
  echo $G2KSRC | gcc -O3 -xc - 2>/dev/null ;
  while read -r m f
  do
    let i++; if [ $i -ge $n ]; then echo -n "." >&2; n=$(( $n + $s )); fi  
    [ $m -eq $MAXINT ] && echo -e "$m\t$f" && continue ;
    echo -e "$(grep -v "^>" $f | ./a.out)\t$f" ;
  done < $SSZ > $TMP
  rm -f a.out ;
fi
sort -g -r $TMP > $SRT ;
rm -f $TMP a.out ;
echo " [ok]" >&2 ;


#############################################################################################################
#############################################################################################################
#### GREEDY CLUSTERING                                                                                   ####
#############################################################################################################
#############################################################################################################

echo >&2 ;
BLANK=""; m=$max; while [ $((--m)) -ne 0 ]; do BLANK="$BLANK "; done
n=$(cat $SRT | wc -l);
i=1;
c=1;
f=$(head -1 $SRT | cut -f2);
while [ ! -e $f.msh ]; do sleep $WAITIME ; done
echo ";$f;" > $CLUSTERS ;
mv $f.msh $MSH ;
sed -i 1d $SRT ;
echo "[$(bc -l<<<"scale=2;100*$i/$n" | sed 's/^\./0./')%] [$c]  +  $f" >&2 ;
while read -r h f
do
  let i++;
  isref=false; if [ $(bc <<<"$h==$MAXINT") -ne 0 ]; then isref=true; fi
  while [ ! -e $f.msh ]; do sleep $WAITIME ; done

  if ! $isref ; then ref=$($MASH dist $MSH $f.msh | sort -g -k3 | awk -v t=$THRESHOLD '($3<=t){print$1}{exit}'); fi

  if $isref || [ -z "$ref" ]
  then
    echo ";$f;" >> $CLUSTERS ;
    $MASH paste $TMP $MSH $f.msh &> /dev/null ;
    mv $TMP.msh $MSH ;
    rm -f $f.msh ;
    let c++;
    echo -e "[$(bc -l<<<"scale=2;100*$i/$n" | sed 's/^\./0./')%] [$c]  +  $f" >&2 ;
    continue;
  fi

  sed -i "s|\(^.*;$ref;.*$\)|\1$f;|" $CLUSTERS ;
  rm -f $f.msh ;
  line="$f$BLANK";
  echo -e "[$(bc -l<<<"scale=2;100*$i/$n" | sed 's/^\./0./')%] [$c]     ${line:0:$max}  ~  $ref" >&2 ;
done < $SRT


#############################################################################################################
#############################################################################################################
#### FINALIZING                                                                                          ####
#############################################################################################################
#############################################################################################################

rm -f $SSZ $SRT $TMP ; 
      
sed -i 's/;/ /g' $CLUSTERS ;
sed -i 's/^ //g' $CLUSTERS ;

echo ;
awk -F" " 'BEGIN{print "cluster\tsize\treference"}{print (++c)"\t"NF"\t"$1}' $CLUSTERS ;
echo ;
echo "details written into $CLUSTERS" ;

exit ;




